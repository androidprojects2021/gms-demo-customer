package com.azinova.gms_customer.models.networkresponse.dashboard;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DashBoardNetworkResponse {

	@SerializedName("graph_detail_days")
	private List<String> graphDetailDays;

	@SerializedName("graph_detail_count")
	private List<Integer> graphDetailCount;

	@SerializedName("dashboard_details")
	private DashboardDetails dashboardDetails;

	@SerializedName("status")
	private String status;

	public List<String> getGraphDetailDays(){
		return graphDetailDays;
	}

	public List<Integer> getGraphDetailCount(){
		return graphDetailCount;
	}

	public DashboardDetails getDashboardDetails(){
		return dashboardDetails;
	}

	public String getStatus(){
		return status;
	}
}