package com.azinova.gms_customer.models.networkresponse.buildinglist;

import com.google.gson.annotations.SerializedName;

public class BuildingsItem{

	@SerializedName("building_id")
	private String buildingId;

	@SerializedName("building_name")
	private String buildingName;

	@SerializedName("code")
	private String code;

	@SerializedName("device_id")
	private Object deviceId;

	@SerializedName("device_type")
	private Object deviceType;

	@SerializedName("is_default")
	private String isDefault;

	@SerializedName("status")
	private String status;

	public String getBuildingId(){
		return buildingId;
	}

	public String getBuildingName(){
		return buildingName;
	}

	public String getCode(){
		return code;
	}

	public Object getDeviceId(){
		return deviceId;
	}

	public Object getDeviceType(){
		return deviceType;
	}

	public String getIsDefault(){
		return isDefault;
	}

	public String getStatus(){
		return status;
	}
}