package com.azinova.gms_customer.models.networkresponse.forceupdate

import com.google.gson.annotations.SerializedName

data class ForceUpdateNetworkResponse(

	@field:SerializedName("android")
	val android: Android? = null,

	@field:SerializedName("ios")
	val ios: Ios? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class Ios(

	@field:SerializedName("new_version")
	val newVersion: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)

data class Android(

	@field:SerializedName("new_version")
	val newVersion: String? = null,

	@field:SerializedName("type")
	val type: String? = null
)
