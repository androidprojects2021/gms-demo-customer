package com.azinova.gms_customer.models.networkresponse.dashboard;

import com.google.gson.annotations.SerializedName;

public class DashboardDetails{

	@SerializedName("rejected_requests")
	private int rejectedRequests;

	@SerializedName("pending_requests")
	private int pendingRequests;

	@SerializedName("total_requests")
	private int totalRequests;

	@SerializedName("accepted_requests")
	private int acceptedRequests;

	@SerializedName("todays_requests")
	private int todaysRequests;

	public int getRejectedRequests(){
		return rejectedRequests;
	}

	public int getPendingRequests(){
		return pendingRequests;
	}

	public int getTotalRequests(){
		return totalRequests;
	}

	public int getAcceptedRequests(){
		return acceptedRequests;
	}

	public int getTodaysRequests(){
		return todaysRequests;
	}
}