package com.azinova.gms_customer.models.networkresponse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Userdetail implements Serializable {



	@SerializedName("user_id")
	private String userId;

	@SerializedName("name")
	private String name;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("location")
	private String location;

	@SerializedName("description")
	private String description;

	@SerializedName("purpose")
	private String purpose;

	@SerializedName("flat_id")
	private String flatId;

	@SerializedName("flat_name")
	private String flat_name;

	@SerializedName("floor_name")
	private String floor_name;

	@SerializedName("image")
	private String image;

	@SerializedName("create_date")
	private String createDate;

	@SerializedName("accepted_status")
	private String acceptedStatus;

	@SerializedName("status")
	private String status;

	@SerializedName("status_message")
	private String status_message;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getFlatId() {
		return flatId;
	}

	public void setFlatId(String flatId) {
		this.flatId = flatId;
	}

	public String getFlat_name() {
		return flat_name;
	}

	public void setFlat_name(String flat_name) {
		this.flat_name = flat_name;
	}

	public String getFloor_name() {
		return floor_name;
	}

	public void setFloor_name(String floor_name) {
		this.floor_name = floor_name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getAcceptedStatus() {
		return acceptedStatus;
	}

	public void setAcceptedStatus(String acceptedStatus) {
		this.acceptedStatus = acceptedStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus_message() {
		return status_message;
	}

	public void setStatus_message(String status_message) {
		this.status_message = status_message;
	}




	/*@SerializedName("image")
	private String image;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("purpose")
	private String purpose;

	@SerializedName("flat_id")
	private String flatId;

	@SerializedName("name")
	private String name;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("accepted_status")
	private String acceptedStatus;

	@SerializedName("description")
	private String description;

	@SerializedName("location")
	private String location;

	@SerializedName("create_date")
	private String createDate;

	@SerializedName("status")
	private String status;


	@SerializedName("flat_num")
	private String flat_num;


	@SerializedName("qatarid")
	private String qatarid;

	@SerializedName("status_message")
	private String status_message;


	public String getStatus_message() {
		return status_message;
	}

	public String getImage(){
		return image;
	}

	public String getUserId(){
		return userId;
	}

	public String getPurpose(){
		return purpose;
	}

	public String getFlatId(){
		return flatId;
	}

	public String getName(){
		return name;
	}

	public String getMobile(){
		return mobile;
	}

	public String getAcceptedStatus(){
		return acceptedStatus;
	}

	public String getDescription(){
		return description;
	}

	public String getLocation(){
		return location;
	}

	public String getCreateDate(){
		return createDate;
	}

	public String getStatus(){
		return status;
	}


	public String getFlat_num() {
		return flat_num;
	}

	public String getQatarid() {
		return qatarid;
	}*/
}