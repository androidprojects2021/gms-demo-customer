package com.azinova.gms_customer.models.networkresponse.pending_request

import com.google.gson.annotations.SerializedName

data class PendingRequestNetworkResponse(

		@field:SerializedName("request_list")
		val requestList: RequestList? = null,

		@field:SerializedName("status")
		val status: String? = null
)

data class RequestList(

	@field:SerializedName("status_message")
	val statusMessage: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("purpose")
	val purpose: String? = null,

	@field:SerializedName("flat_id")
	val flatId: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("accepted_status")
	val acceptedStatus: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("location")
	val location: String? = null,

	@field:SerializedName("create_date")
	val createDate: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("flat_name")
	val flat_name: String? = null,


	@field:SerializedName("floor_name")
	val floor_name: String? = null

	/*@field:SerializedName("flat_num")
	val flat_num: String? = null,


	@field:SerializedName("qatarid")
	val qatarid: String? = null*/




)


/*
"user_id": "51",
"name": "",
"mobile": "11444744",
"location": "",
"description": "",
"purpose": "Visitor",
"flat_id": "1",
"flat_name": "Test employee",
"floor_name": "Test Department",
"image": "",
"create_date": "2021-11-17 14:16:04",
"accepted_status": "0",
"status": "1",
"status_message": "EHTERAZ Verified"*/
