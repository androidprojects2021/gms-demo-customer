package com.azinova.gms_customer.models.networkresponse.history;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class HistoryNetworkResponse{

	@SerializedName("request_list")
	private List<RequestListItem> requestList;

	@SerializedName("status")
	private String status;

	public List<RequestListItem> getRequestList(){
		return requestList;
	}

	public String getStatus(){
		return status;
	}
}