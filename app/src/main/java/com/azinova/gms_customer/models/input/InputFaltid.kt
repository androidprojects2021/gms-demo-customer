package com.azinova.gms_customer.models.input

import com.google.gson.annotations.SerializedName

class InputFaltid {
    @SerializedName("flat_id")
    var flatId: String? = ""
}