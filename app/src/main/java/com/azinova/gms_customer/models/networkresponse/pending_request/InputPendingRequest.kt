package com.azinova.gms_customer.models.networkresponse.pending_request

import com.google.gson.annotations.SerializedName

class InputPendingRequest {
    @SerializedName("flat_id")
    var flatId: String? = ""
}