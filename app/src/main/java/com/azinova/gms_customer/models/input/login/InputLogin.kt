package com.azinova.gms_customer.models.input.login

import com.google.gson.annotations.SerializedName

data class InputLogin(
//{"building_id":"1","device_id":"123FVDFVDV45","device_type":"android","password":"12345","username":"Building1"}
		@field:SerializedName("password")
		val password: String? = null,

		@field:SerializedName("device_id")
		val deviceId: String? = null,

		@field:SerializedName("device_type")
		val deviceType: String? = null,

		@field:SerializedName("username")
		val username: String? = null,

		@field:SerializedName("building_id")
       val building_id: String? = null
)
