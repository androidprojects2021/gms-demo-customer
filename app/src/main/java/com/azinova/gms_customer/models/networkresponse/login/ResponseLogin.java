package com.azinova.gms_customer.models.networkresponse.login;

import com.google.gson.annotations.SerializedName;

public class ResponseLogin{

	@SerializedName("flat_id")
	private String flatId;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	public String getFlatId(){
		return flatId;
	}

	public String getMessage(){
		return message;
	}

	public String getStatus(){
		return status;
	}
}