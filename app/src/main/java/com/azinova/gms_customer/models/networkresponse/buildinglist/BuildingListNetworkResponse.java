package com.azinova.gms_customer.models.networkresponse.buildinglist;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class BuildingListNetworkResponse{

	@SerializedName("buildings")
	private List<BuildingsItem> buildings;

	@SerializedName("status")
	private String status;

	public List<BuildingsItem> getBuildings(){
		return buildings;
	}

	public String getStatus(){
		return status;
	}
}