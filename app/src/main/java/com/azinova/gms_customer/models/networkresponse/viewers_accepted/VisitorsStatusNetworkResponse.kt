package com.azinova.gms_customer.models.networkresponse.viewers_accepted

import com.google.gson.annotations.SerializedName

data class VisitorsStatusNetworkResponse(

	@field:SerializedName("access_token")
	val accessToken: Any? = null,

	@field:SerializedName("notif_result")
	val notifResult: Any? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
