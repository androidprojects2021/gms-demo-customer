package com.azinova.gms_customer.models.networkresponse.viewers_accepted

import com.google.gson.annotations.SerializedName

class InputStatusParam {

    @field:SerializedName("user_id")
    var user_id: String? = null

    @field:SerializedName("accepted_status")
    var accepted_status: String? = null
}