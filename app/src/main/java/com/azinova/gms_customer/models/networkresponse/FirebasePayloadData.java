package com.azinova.gms_customer.models.networkresponse;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FirebasePayloadData implements Serializable {

	@SerializedName("userdetail")
	private Userdetail userdetail;

	public Userdetail getUserdetail(){
		return userdetail;
	}
}