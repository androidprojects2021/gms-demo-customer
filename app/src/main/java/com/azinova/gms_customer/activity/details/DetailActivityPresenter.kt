package com.azinova.gms_customer.activity.details

import android.content.Context
import com.azinova.gms_customer.base.BasePresenter
import com.azinova.gms_customer.models.networkresponse.forceupdate.ForceUpdateNetworkResponse
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.InputStatusParam
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.VisitorsStatusNetworkResponse
import com.azinova.gms_customer.network.NetworkCallback


class DetailActivityPresenter(detailActivityView: DetailActivityView?) : BasePresenter<DetailActivityView?>() {
    init {
        super.attachView(detailActivityView)
    }
    fun getUpdateStatus(context: Context, status: String, userId :String) {

        view!!.showLoader()

        val inputStatusParam = InputStatusParam()
        inputStatusParam.user_id = userId
        inputStatusParam.accepted_status =status

        addSubscribe(apiStores.getVisitorsStatus(inputStatusParam), object : NetworkCallback<VisitorsStatusNetworkResponse?>() {
            override fun onSuccess(model: VisitorsStatusNetworkResponse?) {
                view!!.hideLoader()

                if (model?.status.equals("success")) {


                    view!!.onUpdateSuccess(model!!)

                } else {
                    view?.onError("something went wrong")
                }
            }

            override fun onFailure(message: String?) {
                view!!.hideLoader()

                view?.onError(message)
            }

            override fun onFinish() {

            }
        })

    }

    fun getVersionUpdate() {
        addSubscribe(apiStores.getVersionUpdate(), object : NetworkCallback<ForceUpdateNetworkResponse?>() {
            override fun onSuccess(model: ForceUpdateNetworkResponse?) {
                if (model?.status.equals("success")) {
                    view!!.setUpdateVersionSuccess(model?.android)
                } else {
                    view?.onError(model?.status)
                }
            }
            override fun onFailure(message: String?) {
                view?.onError(message)
            }
            override fun onFinish() {

            }
        })
    }

}