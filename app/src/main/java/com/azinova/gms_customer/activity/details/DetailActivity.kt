package com.azinova.gms_customer.activity.details

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.azinova.gms_customer.BuildConfig
import com.azinova.gms_customer.R
import com.azinova.gms_customer.base.MvpActivity
import com.azinova.gms_customer.fragments.dashboard.DashBoardFragment
import com.azinova.gms_customer.fragments.history.HistoryFragment
import com.azinova.gms_customer.fragments.login.LoginFragment
import com.azinova.gms_customer.models.networkresponse.FirebasePayloadData
import com.azinova.gms_customer.models.networkresponse.Userdetail
import com.azinova.gms_customer.models.networkresponse.forceupdate.Android
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.VisitorsStatusNetworkResponse
import com.azinova.gms_customer.network.ConstantsURL
import com.azinova.gms_customer.utils.ActivityUtils
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.visitors_history_click_alert.view.*

class DetailActivity : MvpActivity<DetailActivityPresenter?>(), DetailActivityView {
    enum class Pages {
        LOGIN,
        DASH_BOARD,
        HISTORY,
    }

    var isFromNotification: Boolean = false
    override fun createPresenter(): DetailActivityPresenter {
        return DetailActivityPresenter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        supportActionBar?.hide();

        presenter!!.getVersionUpdate()



        if (intent.extras != null) {
            for (key in intent.extras!!.keySet()) {
                val value = intent.extras!!.getString(key)
                if (key.equals("data")) {
                    isFromNotification = true
                    val firebasePayloadData = Gson().fromJson(value, FirebasePayloadData::class.java)
                    showDialogue(firebasePayloadData.userdetail)
                }


                Log.d("DetailActivity", "Key: $key Value: $value")


            }
        }

        var page = Pages.DASH_BOARD

        try{
            if (intent != null) {
                if (!isFromNotification) {
                    page = intent.getSerializableExtra("fragmentName") as Pages
                }

            }
        }
        catch (e:Exception){

        }


        loadPage(page)
    }

    private fun loadPage(pages: Pages) {
        when (pages) {
            Pages.LOGIN -> {
                val loginFragment: Fragment = LoginFragment()
                val loginData_bundle = Bundle()
                loginData_bundle.putString("from", intent.getStringExtra("from"))
                loginFragment.arguments = loginData_bundle
                ActivityUtils.replaceFragment(supportFragmentManager, loginFragment, R.id.detailsactivity_content)
            }
            Pages.DASH_BOARD -> {

                val dashBoardFragment: Fragment = DashBoardFragment()
                val otPverificationFragment_bundle = Bundle()

                otPverificationFragment_bundle.putString("from", intent.getStringExtra("from"))

                dashBoardFragment.arguments = otPverificationFragment_bundle
                ActivityUtils.replaceFragment(supportFragmentManager, dashBoardFragment, R.id.detailsactivity_content)
            }
//
            Pages.HISTORY -> {
                val historyFragment: Fragment = HistoryFragment()
                ActivityUtils.replaceFragment(supportFragmentManager, historyFragment, R.id.detailsactivity_content)

            }
//
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            // handling fragment backbutton navigation
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    fun showExitAlert(context: Context?) {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle(R.string.exit)
        builder.setIcon(R.drawable.exit)
        builder.setMessage(R.string.exit_msg)
        //        builder.setIcon(R.drawable.ic_logout);
        builder.setPositiveButton(R.string.yes) { dialog, which ->
            finish()
            System.exit(0)
            //                MainActivity.super.onBackPressed();
            dialog.dismiss()
        }
        builder.setNegativeButton(R.string.no) { dialog, which -> dialog.dismiss() }
        builder.create().show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun showDialogue(buildingsItem: Userdetail) {
        var alertDialog: AlertDialog? = null

        val builder = AlertDialog.Builder(this)
        val view: View = LayoutInflater.from(this).inflate(R.layout.visitors_history_click_alert, null)

        builder.setView(view)
        alertDialog = builder.create()
//        alertDialog!!.setCancelable(false)

        val txtVisitorName = view.findViewById<TextView>(R.id.txtVisitorName)
        val txt_mobile_number = view.findViewById<TextView>(R.id.txt_mobile_number)
        val txt_Location = view.findViewById<TextView>(R.id.txt_Location)
        val txt_name = view.findViewById<TextView>(R.id.txt_name)
        val txt_purpose_of_visit = view.findViewById<TextView>(R.id.txt_purpose_of_visit)
        val profile_image = view.findViewById<ImageView>(R.id.profile_image)
        val txt_status = view.findViewById<TextView>(R.id.txt_status)



        try {
            Glide.with(this).load(buildingsItem.image).centerCrop().placeholder(R.drawable.guest_management_logo).error(R.drawable.guest_management_logo).fallback(R.drawable.guest_management_logo).into(profile_image)
        } catch (e: Exception) {
        }

        txtVisitorName.text = buildingsItem.name
        txt_mobile_number.text = buildingsItem.mobile
        txt_Location.text = buildingsItem.flat_name//.flat_num
        txt_name.text = buildingsItem.status_message//.qatarid
        txt_purpose_of_visit.text = buildingsItem.purpose
        txt_status.text = buildingsItem.status_message


        view.imageView_close.setOnClickListener(View.OnClickListener {
            alertDialog!!.dismiss()
        })

        view.btn_accept.setOnClickListener(View.OnClickListener {
            presenter!!.getUpdateStatus(this, ConstantsURL.ACCEPTED, buildingsItem.userId)
            alertDialog!!.dismiss()
        })

        view.button_reject.setOnClickListener(View.OnClickListener {
            presenter!!.getUpdateStatus(this, ConstantsURL.REJECTED, buildingsItem.userId)
            alertDialog!!.dismiss()

        })

        alertDialog!!.show()
//        }

    }

    override fun showLoader() {

    }

    override fun hideLoader() {
    }

    override fun onUpdateSuccess(model: VisitorsStatusNetworkResponse) {
        Toast.makeText(this, model.message, Toast.LENGTH_SHORT).show()

    }

    override fun onError(message: String?) {
    }

    override fun setUpdateVersionSuccess(android: Android?) {
        if (android?.newVersion!!.toDouble() > BuildConfig.VERSION_NAME.toDouble()) {
            if (android.type.equals("critical")) {
                showCritalUpdateDIalogue(true)
            } else {
                showCritalUpdateDIalogue(false)

            }
        }

    }

    private fun showCritalUpdateDIalogue(isCritical: Boolean) {
        var alertDialog: AlertDialog? = null

        val builder = AlertDialog.Builder(this)
        val view: View = LayoutInflater.from(this).inflate(R.layout.version_update_dialog, null)
        val buton_no_thanks = view.findViewById<Button>(R.id.btnno)
        val button_update = view.findViewById<Button>(R.id.btnYes)

        builder.setView(view)
        alertDialog = builder.create()
        alertDialog?.setCancelable(false)

        if (isCritical) {
            buton_no_thanks.visibility = View.VISIBLE
        } else {
            buton_no_thanks.visibility = View.GONE
        }


        buton_no_thanks.setOnClickListener {
            alertDialog.dismiss()
        }
        button_update.setOnClickListener {

            val viewIntent = Intent("android.intent.action.VIEW",
                    Uri.parse("https://play.google.com/store/apps/details?id=com.azinova.guestmanagment"))
            startActivity(viewIntent)
        }

        alertDialog.show()

    }

//    override fun onResume() {
//
//        super.onResume()
//    }

//    override fun onPause() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(onBroadcastReceiver)
//        super.onPause()
//    }


}

