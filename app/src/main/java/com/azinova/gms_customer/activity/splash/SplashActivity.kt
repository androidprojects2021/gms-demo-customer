package com.azinova.gms_customer.activity.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.azinova.gms_customer.R
import com.azinova.gms_customer.activity.details.DetailActivity
import com.azinova.gms_customer.base.MvpActivity
import com.azinova.gms_customer.utils.ObjectFactory
import com.azinova.limscafeuser.activity.splash.SplashView

class SplashActivity : MvpActivity<SplashPresenter?>(), SplashView {
    var handler: Handler? = null

    override fun createPresenter(): SplashPresenter {
        return SplashPresenter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()
    }

    private fun callNextActivity() {
        handler!!.postDelayed({

            if (ObjectFactory.getInstance(this@SplashActivity).appPreferenceManager.loginStatus) {
                val intent = Intent(this@SplashActivity, DetailActivity::class.java)
                intent.putExtra("fragmentName", DetailActivity.Pages.DASH_BOARD)
                intent.putExtra("from", SPLASH_ACTIVITY)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this@SplashActivity, DetailActivity::class.java)
                intent.putExtra("fragmentName", DetailActivity.Pages.LOGIN)
                intent.putExtra("from", SPLASH_ACTIVITY)
                startActivity(intent)
                finish()

            }
        }, DELAY_TIME.toLong())
    }

    private fun init() {}
    private fun onClick() {}
    override fun onResume() {
        super.onResume()
        if (handler == null) {
            handler = Handler()
        }
        callNextActivity()
    }

    override fun onPause() {
        super.onPause()
        handler!!.removeCallbacksAndMessages(null)
    }

    companion object {
        const val DELAY_TIME = 1000
        const val SPLASHACTIVITY = 12560
        const val SPLASH_ACTIVITY = "SplashActivity"
    }
}