package com.azinova.gms_customer.activity.details

import com.azinova.gms_customer.models.networkresponse.forceupdate.Android
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.VisitorsStatusNetworkResponse

interface DetailActivityView {
    fun showLoader()
    fun hideLoader()
    fun onUpdateSuccess(model: VisitorsStatusNetworkResponse)
    fun onError(message: String?)
    fun setUpdateVersionSuccess (android: Android?)

}