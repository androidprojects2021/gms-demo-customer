package com.azinova.gms_customer.activity.splash

import com.azinova.gms_customer.base.BasePresenter
import com.azinova.limscafeuser.activity.splash.SplashView


public class SplashPresenter(splashView: SplashView?) : BasePresenter<SplashView?>() {
    init {
        super.attachView(splashView)
    }
}