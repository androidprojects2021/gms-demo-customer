package com.azinova.gms_customer.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.azinova.gms_customer.MainActivity
import com.azinova.gms_customer.R
import com.azinova.gms_customer.activity.details.DetailActivity
import com.azinova.gms_customer.models.networkresponse.FirebasePayloadData
import com.azinova.gms_customer.utils.ObjectFactory
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import java.util.*


class FirebaseService : FirebaseMessagingService() {
    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.d("FCM_Token", s)
        ObjectFactory.getInstance(applicationContext).appPreferenceManager.token = s
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d("FCM_Token", Objects.requireNonNull(remoteMessage.data["payload"]))

        val data = remoteMessage.data["payload"]
        val firebasePayloadData = Gson().fromJson(data, FirebasePayloadData::class.java)

        var clickAction: String = ""

        clickAction = remoteMessage.notification?.clickAction ?: ""

        showNotification("sadsd", "dsdsds", firebasePayloadData, clickAction)


    }


    fun showNotification(
        title: String?,
        message: String?,
        datapayload: FirebasePayloadData,
        clickaction: String
    ) {

            if (!NotificationUtils.isAppIsInBackground(applicationContext)) {
                Log.d("FirebaseService", "if")
                Log.d("AppIsInForground", "showNotification: ")
                val intent = Intent("com.gms_fcm__payload")
                intent.putExtra("data", datapayload.userdetail)
                val localBroadcastReceiver = LocalBroadcastManager.getInstance(applicationContext)
                localBroadcastReceiver.sendBroadcast(intent)


            } else {
                message?.let {
                    if (title != null) {
                        Log.d("AppIsInBackground", "showNotification: ")
                        showNotification(title, it, clickaction)
                    }
                }


            }


    }


    fun showNotification(title: String, message: String, clickAction: String) {

        val mNotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                "13",
                "CHANNEL_123",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.description = "CHANNEL_DESCRIPTION"
            mNotificationManager.createNotificationChannel(channel)
        }
        val mBuilder = NotificationCompat.Builder(applicationContext, "YOUR_CHANNEL_ID")
            .setSmallIcon(R.mipmap.ic_launcher) // notification icon
            .setContentTitle(title) // title for notification
            .setContentText(message)// message for notification
            .setAutoCancel(true) // clear notification after click
        val intent = Intent(applicationContext, DetailActivity::class.java)
         intent.putExtra("fragmentName",DetailActivity.Pages.DASH_BOARD)
        Log.d("PendingIntent", "showNotification: ")
        val pi = PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        mBuilder.setContentIntent(pi)
        mNotificationManager.notify(0, mBuilder.build())


/*
        val mBuilder: NotificationCompat.Builder = NotificationCompat.Builder(applicationContext,"13")
            .setSmallIcon(R.mipmap.ic_launcher).
        setContentTitle(title).setContentText(message)
        mBuilder.setAutoCancel(true)
        val resultIntent = Intent(applicationContext, DetailActivity::class.java)
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        resultIntent.putExtra("fragmentName",DetailActivity.Pages.DASH_BOARD)

        // Add the bundle to the intent
        val stackBuilder = TaskStackBuilder.create(applicationContext)
        stackBuilder.addNextIntent(resultIntent)
        val resultPendingIntent: PendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        mBuilder.setContentIntent(resultPendingIntent)
        val alarmSound: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        mBuilder.setSound(alarmSound)
        val mNotificationManager: NotificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mNotificationManager.notify(0, mBuilder.build())*/


    }


}
/* val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
 if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
     val channel = NotificationChannel("13",
             "CHANNEL_123",
             NotificationManager.IMPORTANCE_DEFAULT)
     channel.description = "CHANNEL_DESCRIPTION"
     mNotificationManager.createNotificationChannel(channel)
 }
 val mBuilder = NotificationCompat.Builder(applicationContext, "YOUR_CHANNEL_ID")
         .setSmallIcon(R.mipmap.ic_launcher) // notification icon
         .setContentTitle(title) // title for notification
         .setContentText(message)// message for notification
         .setAutoCancel(true) // clear notification after click
 val intent = Intent(applicationContext, DetailActivity::class.java)
 intent.putExtra("fragmentName",DetailActivity.Pages.DASH_BOARD)

 val stackBuilder = TaskStackBuilder.create(this)

 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
     stackBuilder.addNextIntent(intent)
 }

 val pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
 mBuilder.setContentIntent(pi)
 mNotificationManager.notify(0, mBuilder.build())
}*/



