package com.azinova.gms_customer.fragments.login

import android.content.Context
import com.azinova.gms_customer.base.BasePresenter
import com.azinova.gms_customer.models.input.login.InputLogin
import com.azinova.gms_customer.models.networkresponse.buildinglist.BuildingListNetworkResponse
import com.azinova.gms_customer.models.networkresponse.login.ResponseLogin
import com.azinova.gms_customer.network.NetworkCallback
import com.azinova.gms_customer.utils.ObjectFactory.getInstance

class LoginPresenter(loginView: LoginView?) : BasePresenter<LoginView?>() {
    init {
        super.attachView(loginView)
    }

    fun login(context: Context?, registerData: InputLogin) {
        view!!.showLoader()
        addSubscribe(apiStores.login(registerData), object : NetworkCallback<ResponseLogin?>() {
            override fun onSuccess(model: ResponseLogin?) {
                view!!.hideLoader()

                if (model?.status.equals("success")) {

                    getInstance(context).appPreferenceManager.loginStatus = true
                    getInstance(context).appPreferenceManager.userId = model?.flatId?.toInt() ?: -1
                    getInstance(context).appPreferenceManager.userName = registerData.username



                    view!!.onLoginSuccess()

                } else {
                    view?.onError(model?.message)
                }
            }

            override fun onFailure(message: String?) {
                view!!.hideLoader()

                view?.onError(message)
            }

            override fun onFinish() {

            }
        })
    }

    fun getBuildingList(context: Context?) {
        view!!.showLoader()
        addSubscribe(apiStores.getBuildingList(), object : NetworkCallback<BuildingListNetworkResponse?>() {
            override fun onSuccess(model: BuildingListNetworkResponse?) {
                view!!.hideLoader()

                if (model?.status.equals("success")) {

                    model?.buildings?.let { view!!.setBuildingList(it) }

                } else {
                    view?.onError("Something went wrong")
                }
            }

            override fun onFailure(message: String?) {
                view!!.hideLoader()

                view?.onError(message)
            }

            override fun onFinish() {

            }
        })
    }


}
