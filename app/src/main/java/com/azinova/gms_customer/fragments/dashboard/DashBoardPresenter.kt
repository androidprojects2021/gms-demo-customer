package com.azinova.gms_customer.fragments.dashboard

import android.content.Context
import android.util.Log
import com.azinova.gms_customer.base.BasePresenter
import com.azinova.gms_customer.models.input.InputFaltid
import com.azinova.gms_customer.models.networkresponse.dashboard.DashBoardNetworkResponse
import com.azinova.gms_customer.models.networkresponse.pending_request.InputPendingRequest
import com.azinova.gms_customer.models.networkresponse.pending_request.PendingRequestNetworkResponse
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.InputStatusParam
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.VisitorsStatusNetworkResponse
import com.azinova.gms_customer.network.NetworkCallback
import com.azinova.gms_customer.utils.ObjectFactory

class DashBoardPresenter(dashBoardView: DashBoardView?) : BasePresenter<DashBoardView?>() {
    init {
        super.attachView(dashBoardView)
    }

    fun  getDashBoard(context: Context?) {
        view!!.showLoader()
        val faltid = InputFaltid()
        faltid.flatId = ObjectFactory.getInstance(context).appPreferenceManager.userId.toString()
        addSubscribe(apiStores.getDashBoard(faltid), object : NetworkCallback<DashBoardNetworkResponse?>() {
            override fun onSuccess(model: DashBoardNetworkResponse?) {
                view!!.hideLoader()

                if (model?.status.equals("success")) {
                    view!!.onDashboardSuccess(model)

                } else {
//                    view?.onError(model?.message)
                }
            }

            override fun onFailure(message: String?) {
                view!!.hideLoader()

                view?.onError(message)
            }

            override fun onFinish() {

            }
        })
    }
    fun getUpdateStatus(context: Context, accepted: String,userId :String) {

        view!!.showLoader()

        val inputStatusParam = InputStatusParam()
        inputStatusParam.user_id =  userId
        inputStatusParam.accepted_status =accepted
        Log.d("TAG", "getUpdateStatus: "+userId+","+accepted)
        addSubscribe(apiStores.getVisitorsStatus(inputStatusParam), object : NetworkCallback<VisitorsStatusNetworkResponse?>() {
            override fun onSuccess(model: VisitorsStatusNetworkResponse?) {
                view!!.hideLoader()

                if (model?.status.equals("success")) {


                    view!!.onUpdateSuccess(model!!)

                }else if (model?.status.equals("error")) {

                    view?.onError(model?.message)

                } else {
                    view?.onError("something went wrong")
                }
            }

            override fun onFailure(message: String?) {
                view!!.hideLoader()

                view?.onError(message)
            }

            override fun onFinish() {

            }
        })

    }

    fun getPendingRequest(context: Context) {

        view!!.showLoader()

        val inputPendingRequest = InputPendingRequest()
        inputPendingRequest.flatId = ObjectFactory.getInstance(context).appPreferenceManager.userId.toString()
        Log.d("TAG", "getPendingRequest: "+inputPendingRequest.flatId)

        addSubscribe(apiStores.getPendingRequest(inputPendingRequest), object : NetworkCallback<PendingRequestNetworkResponse?>() {
            override fun onSuccess(model: PendingRequestNetworkResponse?) {
                view!!.hideLoader()

                if (model?.status.equals("success")) {


                    view!!.onpendingSuccess(model!!)

                }
            }

            override fun onFailure(message: String?) {
                view!!.hideLoader()

                view?.onError(message)
            }

            override fun onFinish() {

            }
        })


    }


}
