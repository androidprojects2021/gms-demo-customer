package com.azinova.gms_customer.fragments.dashboard

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.*
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.azinova.gms_customer.R
import com.azinova.gms_customer.activity.details.DetailActivity
import com.azinova.gms_customer.activity.splash.SplashActivity
import com.azinova.gms_customer.base.MvpFragment
import com.azinova.gms_customer.models.networkresponse.Userdetail
import com.azinova.gms_customer.models.networkresponse.buildinglist.BuildingsItem
import com.azinova.gms_customer.models.networkresponse.dashboard.DashBoardNetworkResponse
import com.azinova.gms_customer.models.networkresponse.pending_request.PendingRequestNetworkResponse
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.VisitorsStatusNetworkResponse
import com.azinova.gms_customer.network.ConstantsURL
import com.azinova.gms_customer.utils.ActivityUtils
import com.azinova.gms_customer.utils.ObjectFactory
import com.bumptech.glide.Glide
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.layout_loader.*
import kotlinx.android.synthetic.main.visitors_history_click_alert.view.*

class DashBoardFragment : MvpFragment<DashBoardPresenter?>(), DashBoardView {
    private var rootView: View? = null
    var FROM: String = ""
    var alertDialog: AlertDialog? = null
    lateinit var buildings: List<BuildingsItem>
    lateinit var building: BuildingsItem
    var emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    var barChart: BarChart? = null

    lateinit var Model:DashBoardNetworkResponse

    var from: String = ""
    override fun createPresenter(): DashBoardPresenter {
        return DashBoardPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false).also { rootView = it }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let { LocalBroadcastManager.getInstance(it).registerReceiver(mHandler, IntentFilter("com.gms_fcm__payload")) }

        init()
        onClick()

        txt_flat.text = "Welcome ! " +  ObjectFactory.getInstance(context).appPreferenceManager.userName
    }

    private fun init() {
        FROM = arguments?.getString("from").toString()

        barChart =  rootView?.findViewById(R.id.barchart) as BarChart

        /*val entries: ArrayList<BarEntry> = ArrayList()
        entries.add(BarEntry(38f, 0))
        entries.add(BarEntry(52f, 1))
        entries.add(BarEntry(65f, 2))
        entries.add(BarEntry(30f, 3))
        entries.add(BarEntry(85f, 4))
        entries.add(BarEntry(19f, 5))
        entries.add(BarEntry(75f, 6))

        val bardataset = BarDataSet(entries, " ")

        val labels = ArrayList<String>()

        labels.add("Mon");
        labels.add("Tue");
        labels.add("Wed");
        labels.add("Thu");
        labels.add("Fri");
        labels.add("Sat");
        labels.add("Sun");

        val data = BarData(labels, bardataset)*/

        //barChart.data = data

        //bardataset.color = ContextCompat.getColor(context!!, R.color.theme_color)
            //barChart.getAxisLeft().setDrawGridLines(false);
        barChart!!.getXAxis().setTextColor(ContextCompat.getColor(context!!,R.color.theme_color));
        barChart!!.getXAxis().setDrawGridLines(false);
        barChart!!.getAxisRight().setDrawGridLines(false);
        barChart!!.getAxisRight().setEnabled(false)

        barChart!!.setHighlightPerTapEnabled(false)
        barChart!!.setTouchEnabled(false)
        barchart.setPinchZoom(false);
        barchart.setDescription(null);
        val leg: Legend = barChart!!.getLegend()
        leg.setEnabled(false)

        barChart!!.getAxisLeft().setTextColor(ContextCompat.getColor(context!!, R.color.theme_color)); // left y-axis
        presenter?.getDashBoard(context)

        presenter?.getPendingRequest(context!!)

    }

    private fun onClick() {

        btn_goto_requests.setOnClickListener {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("fragmentName", DetailActivity.Pages.HISTORY)
            intent.putExtra("from", SplashActivity.SPLASH_ACTIVITY)
            startActivity(intent)
        }

        logout_icon.setOnClickListener(View.OnClickListener {
            showLogoutAlert(this!!.context!!)
        })

        view?.isFocusableInTouchMode = true
        view?.requestFocus()
        view?.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                showExitAlert(context)
                true
            } else false
        })
    }


    fun showExitAlert(context: Context?) {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle(R.string.exit)
        builder.setIcon(R.drawable.exit)
        builder.setMessage(R.string.exit_msg)
        //        builder.setIcon(R.drawable.ic_logout);
        builder.setPositiveButton(R.string.yes) { dialog, which ->
            getActivity()!!.finish()
            System.exit(0)
            //                MainActivity.super.onBackPressed();
            dialog.dismiss()
        }
        builder.setNegativeButton(R.string.no) { dialog, which -> dialog.dismiss() }
        builder.create().show()
    }

    override fun showLoader() {
        linearLayoutLoaderView.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        linearLayoutLoaderView.visibility = View.GONE

    }

    override fun onError(message: String?) {
        ActivityUtils.showErrorToast(message, rootView)
    }

    override fun onDashboardSuccess(model: DashBoardNetworkResponse?) {
        Log.d("TAG", "onDashboardSuccess: "+model)
        Model= model!!
        todays_request_count.text = model!!.dashboardDetails.todaysRequests.toString()
        rejected_requests_count.text = model!!.dashboardDetails.rejectedRequests.toString()
        accepted_requests_count.text = model!!.dashboardDetails.acceptedRequests.toString()
        total_request_count.text = model!!.dashboardDetails.totalRequests.toString()
        pending_request_count.text = model!!.dashboardDetails.pendingRequests.toString()

        val labels = Model!!.graphDetailDays
        val counts = Model!!.graphDetailCount

        val entries: ArrayList<BarEntry> = ArrayList()

        var index = 0
        for (item in counts) {
            entries.add(BarEntry(item.toFloat(), index))
            index++
        }

        val bardataset = BarDataSet(entries, " ")
        bardataset.color = ContextCompat.getColor(context!!, R.color.theme_color)
        val data = BarData(labels, bardataset)
        barChart!!.data = data
        barChart!!.animateY(2000)
    }

    override fun onUpdateSuccess(model: VisitorsStatusNetworkResponse) {

    }

    override fun onpendingSuccess(model: PendingRequestNetworkResponse) {
        if(model.requestList !=null){
            showPendingAlert(model)
        }
    }

    private fun showPendingAlert(model: PendingRequestNetworkResponse) {

        val builder = AlertDialog.Builder(requireContext())
        val view: View = LayoutInflater.from(context).inflate(R.layout.visitors_history_click_alert, null)

        builder.setView(view)
        alertDialog = builder.create()

        val txtVisitorName = view.findViewById<TextView>(R.id.txtVisitorName)
        val txt_mobile_number = view.findViewById<TextView>(R.id.txt_mobile_number)
        val txt_Location = view.findViewById<TextView>(R.id.txt_Location)
        val txt_name = view.findViewById<TextView>(R.id.txt_name)
        val txt_status = view.findViewById<TextView>(R.id.txt_status)
        val txt_purpose_of_visit = view.findViewById<TextView>(R.id.txt_purpose_of_visit)
        val profile_image = view.findViewById<ImageView>(R.id.profile_image)

        try {
            Glide.with(requireContext()).load(model.requestList!!.image).centerCrop().placeholder(R.drawable.no_image).error(R.drawable.no_image).fallback(R.drawable.no_image).into(profile_image)
        } catch (e: Exception) {
        }

        txtVisitorName.text = model.requestList!!.name
        txt_mobile_number.text = model.requestList!!.mobile
        txt_Location.text = model.requestList!!.flat_name    //.flat_num
        txt_name.text = model.requestList!!.statusMessage    //.qatarid
        txt_status.text = model.requestList!!.statusMessage
        txt_purpose_of_visit.text = model.requestList!!.purpose

        view.imageView_close.setOnClickListener(View.OnClickListener {
            alertDialog!!.dismiss()
        })

        view.btn_accept.setOnClickListener(View.OnClickListener {
            presenter?.getUpdateStatus(requireContext(), ConstantsURL.ACCEPTED, model.requestList.userId!!)
            alertDialog!!.dismiss()
        })

        view.button_reject.setOnClickListener(View.OnClickListener {
            presenter?.getUpdateStatus(requireContext(), ConstantsURL.REJECTED, model.requestList.userId!!)
            alertDialog!!.dismiss()

        })

        alertDialog!!.show()

    }

    fun showLogoutAlert(context: Context) {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle(R.string.Logout)
        builder.setMessage(R.string.logout_msg)
        builder.setIcon(R.drawable.ic_logout)
        builder.setPositiveButton(R.string.yes, DialogInterface.OnClickListener { dialog, which ->
            ObjectFactory.getInstance(context).getAppPreferenceManager().setLoginStatus(false)
            ObjectFactory.getInstance(context).getAppPreferenceManager().setUserId(-1)
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("fragmentName", DetailActivity.Pages.LOGIN)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
            dialog.dismiss()
        })
        builder.setNegativeButton(R.string.no, DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
        builder.create().show()
    }

    //NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    // manager.cancel(notificationId);


    private val mHandler: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val firebasePayloadData = intent.getSerializableExtra("data") as Userdetail
            showDialogue(firebasePayloadData)
        }
    }

    fun showDialogue(buildingsItem:Userdetail) {
        val builder = AlertDialog.Builder(context!!)
        val view: View = LayoutInflater.from(context).inflate(R.layout.visitors_history_click_alert, null)

        builder.setView(view)
        alertDialog = builder.create()
        alertDialog!!.setCancelable(false)

        val txtVisitorName = view.findViewById<TextView>(R.id.txtVisitorName)
        val txt_mobile_number = view.findViewById<TextView>(R.id.txt_mobile_number)
        val txt_Location = view.findViewById<TextView>(R.id.txt_Location)
        val txt_name = view.findViewById<TextView>(R.id.txt_name)
        val txt_purpose_of_visit = view.findViewById<TextView>(R.id.txt_purpose_of_visit)
        val profile_image = view.findViewById<ImageView>(R.id.profile_image)
        val txt_status = view.findViewById<TextView>(R.id.txt_status)


        try {
            Glide.with(context!!).load(buildingsItem.image).centerCrop().placeholder(R.drawable.no_image).error(R.drawable.no_image).fallback(R.drawable.no_image).into(profile_image)
        } catch (e: Exception) {
        }

        txtVisitorName.text =buildingsItem.name
        txt_mobile_number.text =buildingsItem.mobile
        txt_Location.text =buildingsItem.flat_name
        txt_name.text =buildingsItem.description
        txt_purpose_of_visit.text =buildingsItem.purpose
        txt_status.text = buildingsItem.status_message

        view. imageView_close.setOnClickListener(View.OnClickListener {
            alertDialog!!.dismiss()
        })

        view.btn_accept.setOnClickListener(View.OnClickListener {
            presenter!!.getUpdateStatus(context!!, ConstantsURL.ACCEPTED,buildingsItem.userId)
            alertDialog!!.dismiss()
        })

        view.button_reject.setOnClickListener(View.OnClickListener {
            presenter!!.getUpdateStatus(context!!, ConstantsURL.REJECTED,buildingsItem.userId)
            alertDialog!!.dismiss()

        })
            alertDialog!!.show()
//        }

    }
}