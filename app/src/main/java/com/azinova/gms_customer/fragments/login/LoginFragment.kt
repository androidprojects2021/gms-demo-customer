package com.azinova.gms_customer.fragments.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.azinova.gms_customer.R
import com.azinova.gms_customer.activity.details.DetailActivity
import com.azinova.gms_customer.base.MvpFragment
import com.azinova.gms_customer.models.input.login.InputLogin
import com.azinova.gms_customer.models.networkresponse.buildinglist.BuildingsItem
import com.azinova.gms_customer.utils.ActivityUtils
import com.azinova.gms_customer.utils.ObjectFactory
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.layout_loader.*


class LoginFragment : MvpFragment<LoginPresenter?>(), LoginView,BuildingListAdapter.ClickBuildingItem {
    private var rootView: View? = null
    var FROM: String = ""
    var alertDialog: AlertDialog? = null
    lateinit var  buildings: List<BuildingsItem>
    lateinit var  building: BuildingsItem
    var emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    var from: String = ""
    override fun createPresenter(): LoginPresenter {
        return LoginPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false).also { rootView = it }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        onClick()
    }

    private fun init() {


        FROM = arguments?.getString("from").toString()


        presenter?.getBuildingList(context)
    }

    private fun onClick() {

        login_button.setOnClickListener {

            if (user_name_building.text.isEmpty()) {
                user_name_building.error = "required"
            } else if (password_input.text.isEmpty()) {
                password_input.error = "required"
            } else if (user_name_input.text.isEmpty()) {
                user_name_input.error = "Invalid user name"

            } else if (password_input.text.length < 2) {
                password_input.error = "Password should be 6 characters"
            } else {
                presenter?.login(context, InputLogin(
                        password = password_input.text.toString(),
                        username = user_name_input.text.toString(),
                        deviceType = "android",
                        deviceId = ObjectFactory.getInstance(context).appPreferenceManager.token,
                        building_id = building.buildingId)

                        )
            }
        }

        user_name_building.setOnClickListener {

            showBuildingList()
        }
    }

    override fun showLoader() {
        linearLayoutLoaderView.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        linearLayoutLoaderView.visibility = View.GONE

    }

    override fun onError(message: String?) {
        ActivityUtils.showErrorToast(message, rootView)
    }


    override fun onLoginSuccess() {
            Toast.makeText(context, "Logged in Successfully", Toast.LENGTH_SHORT).show()
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("fragmentName", DetailActivity.Pages.DASH_BOARD)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
    }


    override fun setBuildingList(buildings: List<BuildingsItem>) {
        this.buildings = buildings

    }


    private fun showBuildingList() {
        val builder = AlertDialog.Builder(context!!)
        val view: View = LayoutInflater.from(context).inflate(R.layout.buildinglistdioalogue, null)
        val recycleview_building = view.findViewById<RecyclerView>(R.id.recycleview_building)

        if (buildings!=null){
            recycleview_building.adapter = BuildingListAdapter(context!!,buildings,this)
            builder.setView(view)
            alertDialog = builder.create()
            alertDialog!!.show()

        }

    }

    override fun onBuildingSelected(buildingsItem: BuildingsItem) {
        this.building = buildingsItem
        user_name_building.text= building.buildingName
        alertDialog!!.dismiss()

    }
}