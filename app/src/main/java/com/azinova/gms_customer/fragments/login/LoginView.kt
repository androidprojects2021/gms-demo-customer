package com.azinova.gms_customer.fragments.login

import com.azinova.gms_customer.models.networkresponse.buildinglist.BuildingsItem

interface LoginView {
    fun showLoader()
    fun hideLoader()
    fun onError(message: String?)
    fun onLoginSuccess()
    fun setBuildingList(buildings: List<BuildingsItem>)
}