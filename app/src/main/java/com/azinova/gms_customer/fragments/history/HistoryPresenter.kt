package com.azinova.gms_customer.fragments.history

import android.content.Context
import com.azinova.gms_customer.base.BasePresenter
import com.azinova.gms_customer.models.input.InputFaltid
import com.azinova.gms_customer.models.networkresponse.history.HistoryNetworkResponse
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.InputStatusParam
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.VisitorsStatusNetworkResponse
import com.azinova.gms_customer.network.NetworkCallback
import com.azinova.gms_customer.utils.ObjectFactory

class HistoryPresenter(historyView: HistoryView?) : BasePresenter<HistoryView?>() {
    init {
        super.attachView(historyView)
    }

    fun getHistory(context: Context?) {
        view!!.showLoader()

      val faltid = InputFaltid()
        faltid.flatId = ObjectFactory.getInstance(context).appPreferenceManager.userId.toString()


        addSubscribe(apiStores.getHistoryList(faltid), object : NetworkCallback<HistoryNetworkResponse?>() {
            override fun onSuccess(model: HistoryNetworkResponse?) {
                view!!.hideLoader()

                if (model?.status.equals("success")) {


                    view!!.setHistoryList(model?.requestList)

                }
                else {
                    view?.onError("some thing went wrong")
                }
            }

            override fun onFailure(message: String?) {
                view!!.hideLoader()

                view?.onError(message)
            }

            override fun onFinish() {

            }
        })
    }

    fun getUpdateStatus(context: Context, status: String,userId :String) {

        view!!.showLoader()

        val inputStatusParam = InputStatusParam()
        inputStatusParam.user_id = userId
        inputStatusParam.accepted_status =status

        addSubscribe(apiStores.getVisitorsStatus(inputStatusParam), object : NetworkCallback<VisitorsStatusNetworkResponse?>() {
            override fun onSuccess(model: VisitorsStatusNetworkResponse?) {
                view!!.hideLoader()

                if (model?.status.equals("success")) {


                    view!!.onUpdateSuccess(model!!)
                }

//                } else {
//                    view?.onError("something went wrong")
//                }
            }

            override fun onFailure(message: String?) {
                view!!.hideLoader()

                view?.onError(message)
            }

            override fun onFinish() {

            }
        })

    }



}
