package com.azinova.gms_customer.fragments.login

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azinova.gms_customer.R
import com.azinova.gms_customer.models.networkresponse.buildinglist.BuildingsItem
import kotlinx.android.synthetic.main.custom_building_view.view.*

class BuildingListAdapter(val context: Context, val buildingsItem: List<BuildingsItem> = ArrayList(), val clickBuildingItem: ClickBuildingItem) : RecyclerView.Adapter<BuildingListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BuildingListAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.custom_building_view, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: BuildingListAdapter.ViewHolder, position: Int) {

        holder.itemView.text_building.text = buildingsItem[position].buildingName

        holder.itemView.setOnClickListener {
            clickBuildingItem.onBuildingSelected(buildingsItem[position])
        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    override fun getItemCount(): Int {
        return buildingsItem.size
    }

    interface ClickBuildingItem {
        fun onBuildingSelected(buildingsItem: BuildingsItem)
    }


}