package com.azinova.gms_customer.fragments.history

import com.azinova.gms_customer.models.networkresponse.history.RequestListItem
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.VisitorsStatusNetworkResponse

interface HistoryView {
    fun showLoader()
    fun hideLoader()
    fun onError(message: String?)
    fun setHistoryList(requestList: List<RequestListItem>?)
    fun onUpdateSuccess(model:VisitorsStatusNetworkResponse )

}