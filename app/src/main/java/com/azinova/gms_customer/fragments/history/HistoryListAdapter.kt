package com.azinova.gms_customer.fragments.history

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.azinova.gms_customer.R
import com.azinova.gms_customer.models.networkresponse.history.RequestListItem
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.visitors_history_list.view.*

class HistoryListAdapter(val context: Context, private val buildingsItem: List<RequestListItem> = ArrayList(), val clickBuildingItem: ClickBuildingItem) : RecyclerView.Adapter<HistoryListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryListAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.visitors_history_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: HistoryListAdapter.ViewHolder, position: Int) {
        Log.e("TAG", "setHistoryList: "+buildingsItem[position])

       /* Flat = department name
          Floor = employee name*/

        val  item =  buildingsItem[position]

        holder.itemView.txt_visitor_office.text = item.flat_name  //item.flat_num
        holder.itemView.txt_visitor_mobile.text =item.mobile
        holder.itemView.txt_visit_purpose.text =item.purpose
        holder.itemView.txt_status.text =item.status_message
        holder.itemView.name.text =item.status_message    //item.qatarid
        holder.itemView.txtVisitorName.text =item.name

        try {
            Glide.with(context).load(item.image).centerCrop().placeholder(R.drawable.no_image).error(R.drawable.no_image).fallback(R.drawable.no_image).into(holder.itemView.profile_image)
        } catch (e: Exception) {
        }
        holder.itemView.setOnClickListener {
            clickBuildingItem.onBuildingSelected(item)

        }

        if(item.acceptedStatus == "1"){
          holder.itemView.txt_status.text = "Accepted"
            holder.itemView.txt_status.setTextColor(ContextCompat.getColor(context, R.color.green))
            holder.itemView.img_status.background = ContextCompat.getDrawable(context, R.drawable.accepted_icon)
        }else if(item.acceptedStatus == "0"){
            holder.itemView.txt_status.text = "Pending"
            holder.itemView.txt_status.setTextColor(ContextCompat.getColor(context, R.color.orange))
            holder.itemView.img_status.background = ContextCompat.getDrawable(context, R.drawable.pending)
        }else if(item.acceptedStatus == "2"){
            holder.itemView.txt_status.text = "Rejected"
            holder.itemView.txt_status.setTextColor(ContextCompat.getColor(context, R.color.rejected))
            holder.itemView.img_status.background = ContextCompat.getDrawable(context, R.drawable.reject_icon)
        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    }

    override fun getItemCount(): Int {
        return buildingsItem.size
    }

    interface ClickBuildingItem {
        fun onBuildingSelected(buildingsItem: RequestListItem)

    }


}