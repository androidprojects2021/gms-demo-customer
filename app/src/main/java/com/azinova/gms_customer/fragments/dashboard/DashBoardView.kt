package com.azinova.gms_customer.fragments.dashboard

import com.azinova.gms_customer.models.networkresponse.dashboard.DashBoardNetworkResponse
import com.azinova.gms_customer.models.networkresponse.pending_request.PendingRequestNetworkResponse
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.VisitorsStatusNetworkResponse

interface DashBoardView {
    fun showLoader()
    fun hideLoader()
    fun onError(message: String?)
    fun onDashboardSuccess(model: DashBoardNetworkResponse?)
    fun onUpdateSuccess(model: VisitorsStatusNetworkResponse)
    fun onpendingSuccess(model: PendingRequestNetworkResponse)

}