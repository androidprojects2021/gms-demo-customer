package com.azinova.gms_customer.fragments.history

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.azinova.gms_customer.R
import com.azinova.gms_customer.base.MvpFragment
import com.azinova.gms_customer.models.networkresponse.buildinglist.BuildingsItem
import com.azinova.gms_customer.models.networkresponse.history.RequestListItem
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.VisitorsStatusNetworkResponse
import com.azinova.gms_customer.network.ConstantsURL
import com.azinova.gms_customer.utils.ActivityUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fargment_viewers_history.*
import kotlinx.android.synthetic.main.layout_loader.*
import kotlinx.android.synthetic.main.visitors_history_click.view.*
import kotlinx.android.synthetic.main.visitors_history_click_alert.view.btn_accept
import kotlinx.android.synthetic.main.visitors_history_click_alert.view.button_reject
import kotlinx.android.synthetic.main.visitors_history_click_alert.view.imageView_close


class HistoryFragment : MvpFragment<HistoryPresenter?>(), HistoryView, HistoryListAdapter.ClickBuildingItem {
    private var rootView: View? = null
    var FROM: String = ""
    var alertDialog: AlertDialog? = null
    lateinit var buildings: List<BuildingsItem>
    lateinit var building: BuildingsItem
    var emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"

    var from: String = ""
    override fun createPresenter(): HistoryPresenter {
        return HistoryPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fargment_viewers_history, container, false).also { rootView = it }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        onClick()
    }

    private fun init() {
        FROM = arguments?.getString("from").toString()

        presenter?.getHistory(context)

    }

    private fun onClick() {
        backArrowVisitorsHistory.setOnClickListener(View.OnClickListener {
            getActivity()?.onBackPressed()
        })

    }

    override fun showLoader() {
        linearLayoutLoaderView.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        linearLayoutLoaderView.visibility = View.GONE

    }

    override fun onError(message: String?) {
        ActivityUtils.showErrorToast(message, rootView)
    }

    override fun setHistoryList(requestList: List<RequestListItem>?) {
        visitorsHistoryRecycler.adapter = requestList?.let { context?.let { it1 -> HistoryListAdapter(it1, it, this) } }
    }

    override fun onUpdateSuccess(model: VisitorsStatusNetworkResponse) {
        Toast.makeText(context, model.message, Toast.LENGTH_SHORT).show()
        presenter?.getHistory(context)

    }

    override fun onBuildingSelected(buildingsItem: RequestListItem) {
        val builder = AlertDialog.Builder(requireContext())
        val view: View = LayoutInflater.from(context).inflate(R.layout.visitors_history_click, null)

        builder.setView(view)
        alertDialog = builder.create()

        val txtVisitorName = view.findViewById<TextView>(R.id.txtVisitorName)
        val txt_mobile_number = view.findViewById<TextView>(R.id.txt_mobile_number)
        val txt_Location = view.findViewById<TextView>(R.id.txt_Location)
        val txt_name = view.findViewById<TextView>(R.id.txt_name)
        val txt_status = view.findViewById<TextView>(R.id.txt_status)
        val txt_purpose_of_visit = view.findViewById<TextView>(R.id.txt_purpose_of_visit)
        val profile_image = view.findViewById<ImageView>(R.id.profile_image)

        try {
            Glide.with(requireContext()).load(buildingsItem.image).centerCrop().placeholder(R.drawable.no_image).error(R.drawable.no_image).fallback(R.drawable.no_image).into(profile_image)
        } catch (e: Exception) {
        }

        if (buildingsItem.acceptedStatus=="0"){
            view.llbtn.visibility=View.VISIBLE
        }

        txtVisitorName.text = buildingsItem.name
        txt_mobile_number.text = buildingsItem.mobile
        txt_Location.text = buildingsItem.flat_name   //.flat_num
        txt_name.text = buildingsItem.description
        txt_status.text = buildingsItem.status_message
        txt_purpose_of_visit.text = buildingsItem.purpose

        view.imageView_close.setOnClickListener(View.OnClickListener {
            alertDialog!!.dismiss()
        })

        view.btn_accept.setOnClickListener(View.OnClickListener {
            presenter?.getUpdateStatus(requireContext(), ConstantsURL.ACCEPTED,buildingsItem.userId)
            alertDialog!!.dismiss()
        })

        view.button_reject.setOnClickListener(View.OnClickListener {
            presenter?.getUpdateStatus(requireContext(), ConstantsURL.REJECTED,buildingsItem.userId)
            alertDialog!!.dismiss()

        })

        alertDialog!!.show()
    }

    private fun showBuildingList() {

    }

}