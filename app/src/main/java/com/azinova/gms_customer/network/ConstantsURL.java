package com.azinova.gms_customer.network;

public class ConstantsURL {


   // public static final String BASE_URL = "http://demo.emaid.info:8083/gms-demo/api/";//demo url
   public static final String BASE_URL = "https://prod.azinova.ae:2443/guesster/gms-demo/api/";  // new url - 29/06/2022

 /* old url : http://demo.emaid.info:8083/gms-demo
    New url : https://prod.azinova.ae:2443/guesster/gms-demo/
    */

    public static final String ACCEPTED =  "1";
    public static final String REJECTED =  "2";


}
