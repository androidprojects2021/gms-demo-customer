package com.azinova.gms_customer.network

import com.azinova.gms_customer.models.input.InputFaltid
import com.azinova.gms_customer.models.input.login.InputLogin
import com.azinova.gms_customer.models.networkresponse.buildinglist.BuildingListNetworkResponse
import com.azinova.gms_customer.models.networkresponse.dashboard.DashBoardNetworkResponse
import com.azinova.gms_customer.models.networkresponse.forceupdate.ForceUpdateNetworkResponse
import com.azinova.gms_customer.models.networkresponse.history.HistoryNetworkResponse
import com.azinova.gms_customer.models.networkresponse.login.ResponseLogin
import com.azinova.gms_customer.models.networkresponse.pending_request.InputPendingRequest
import com.azinova.gms_customer.models.networkresponse.pending_request.PendingRequestNetworkResponse
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.InputStatusParam
import com.azinova.gms_customer.models.networkresponse.viewers_accepted.VisitorsStatusNetworkResponse
import retrofit2.http.Body
import retrofit2.http.POST
import rx.Observable

interface NetworkStores {

    @POST("user_login")
    fun login(@Body inputLogin: InputLogin?): Observable<ResponseLogin?>?

    @POST("list_buildings")
    fun getBuildingList(): Observable<BuildingListNetworkResponse?>?

    @POST("list_requests_byflat")
    fun getHistoryList(@Body input: InputFaltid): Observable<HistoryNetworkResponse?>?

    @POST("dashboard")
    fun getDashBoard(@Body input: InputFaltid): Observable<DashBoardNetworkResponse?>?

    @POST("update_request_status")
    fun getVisitorsStatus(@Body input: InputStatusParam): Observable<VisitorsStatusNetworkResponse?>?

    @POST("update_request_status")
    fun getVisitorsRejected(@Body inputStatusParam:InputStatusParam ): Observable<VisitorsStatusNetworkResponse?>?

    @POST("pending_request")
    fun getPendingRequest(@Body input: InputPendingRequest): Observable<PendingRequestNetworkResponse?>?


    @POST("customer_app_update")
    fun getVersionUpdate(): Observable<ForceUpdateNetworkResponse?>?


}